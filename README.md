# vs_code_settings
prebacivanje sa atom na vs code - podesavanja za vs code 

![ide](img/hackerman.gif?raw=true "Title")


**Podsetnik**
- ```Ctrl+Shift+p``` za ulaz na Command Palette
- ```Ctrl+j ``` za terminal
- ``` ```
- ``` ```


**Platforma** 
- ```lsb_release -r && uname -r ```  
```LTS 18.04 14.*```

**Install bez rasmisljanja™**

[1] dl ink [ovde] (https://code.visualstudio.com/) ili ``` wget NAZIV_FAJLA```

[2] ```$ cd   ~/Downloads ```

[3] ```$ chmod +x  NAZIV_FAJLA```

[4] ```$ sudo dpkg -i NAZIV_FAJLA```

[5] ```$ chmod -x NAZIV_FAJLA ``` ili ```rmrf``` it 

**Basic konfiguracija bez razmisljanja™**

[1] remove auto-update da ne sjb nesto  ```File > Preferences > Setting``` u search-box update channel 
- Update:Channel to nope 
- Update: Enable Windows Background Updates to nope 
- restart kad trazi 

[2] remove telemetry  ```File > Preferences > Setting``` u search-box telemetry 
- Telemetry: Enable Telemetry to nope
- Telemetry: Enable Crash Reporter to nope 
- restart kad trazi 


**Ekstenzije kada previs bloatware** 

- python 
- CMake Tools 
- Spell Checker 
- emmet
- TensorFlow Snippets  
- Material Icon Theme
- pylint u terminal python -m pip install -U pylint

**Teme** 
- ```Ctrl+Shift+p``` za komandnu liniju 
- u search cobalt theme (naj za oci ) 



